import React, { useState, useEffect } from 'react';
import { getConfigValue } from '@ijl/cli';
import Lottie from 'react-lottie';
import { Navigate } from 'react-router-dom';
import clsx from 'clsx';

import {
    Igor,
    Petr,
    Vlad,
    Artem,
    Maks,
    Rus,
    Yulya,
    Bogdan,
    Me,
    Vadim,
    Andrey,
    lottieAnimationDatas,
} from '../../assets';

import * as classNames from './styled';
import { useEvents } from '../../hooks/events';
import { URLs } from '../../__data__/urls';
import { LOCALSTORAGE_NICK_NAME } from '../../__data__/constants';

const avas = [
    { name: 'Me', src: Me },
    { name: 'Yulya', src: Yulya },
    { name: 'Artem', src: Artem },
    { name: 'Petr', src: Petr },
    { name: 'Maks', src: Maks },
    { name: 'Vlad', src: Vlad },
    { name: 'Igor', src: Igor },
    { name: 'Bogdan', src: Bogdan },
    { name: 'Andrey', src: Andrey },
    { name: 'Rus', src: Rus },
    { name: 'Vadim', src: Vadim },
];

const zoomHref = getConfigValue('zoom-bar.zoom.link');

export const Auth = () => {
    const { online } = useEvents();
    const [clickedUser, setClickeduser] = useState(null);
    const [isPlaing, setIsPlaying] = useState(false);
    const [donePlaying, setDonePlaying] = useState(false);

    const handleClick = event => {
        const username = event.currentTarget.dataset.userName;
        if (!online[username]?.online) {
            setClickeduser(username);
        }
    };

    useEffect(() => {
        if (online[clickedUser]?.online) {
            setClickeduser(null);
        }
    }, [online]);

    const handleClickEnter = () => {
        if (clickedUser) {
            setIsPlaying(true);
            fetch(`${getConfigValue('zoom-bar.api')}/enter`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                },
                body: JSON.stringify({ username: clickedUser }),
            });
            localStorage.setItem(LOCALSTORAGE_NICK_NAME, clickedUser);
        }
    };

    const handleEnter2Bar = () => {
        setDonePlaying(true);
    };

    if (donePlaying) {
        return <Navigate replace to={URLs.bar.url} />;
    }

    return (
        <div className={classNames.Wrapper}>
            <h2>Приветствую в нашем Zoom баре</h2>
            <h2>
                Выбирайте свою аватарку и проходите в zoom по{' '}
                <a rel="noreferrer nofollow" href={zoomHref} target="_blank">
                    ссылке
                </a>
            </h2>

            <main className={clsx(classNames.AvasPaper, classNames.AvasGrid)}>
                {avas.map((a, index) => (
                    <div
                        className={classNames.AvaWrapper}
                        key={a.src}
                        style={{
                            gridArea: `ava${index}`,
                        }}
                    >
                        {clickedUser === a.name && (
                            <div className={classNames.DarkCircle}>
                                <span>Вот он я</span>
                            </div>
                        )}
                        {online[a.name]?.online && (
                            <div className={classNames.DarkCircle}>
                                <span>уже тут</span>
                            </div>
                        )}
                        <button
                            data-user-name={a.name}
                            onClick={handleClick}
                            className={classNames.DropButtonStyled}
                        >
                            <img className={classNames.AvaImg} src={a.src} />
                        </button>
                    </div>
                ))}
            </main>

            <button className={classNames.Button} onClick={handleClickEnter}>
                Войти
            </button>

            {isPlaing && (
                <div className={classNames.LottieWrapper}>
                    <div className={classNames.WiteCirlce} />
                    <Lottie
                        options={{
                            loop: true,
                            autoplay: true,
                            animationData: lottieAnimationDatas.friday,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid slice',
                            },
                        }}
                        eventListeners={[
                            {
                                eventName: 'loopComplete',
                                callback: handleEnter2Bar,
                            },
                        ]}
                        height={400}
                        width={400}
                    />
                </div>
            )}
        </div>
    );
};
