import { css } from '@emotion/css';

export const Wrapper = css`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const AvasPaper = css`
    background: #ffffff;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.25);
    padding: 24px;
    margin-top: 64px;
`;

export const AvaWrapper = css`
    align-self: center;
    justify-self: center;
    transition: filter 0.4s;
    position: relative;

    &:hover {
        filter: drop-shadow(1px 1px 5px rgba(0, 0, 0, 0.59));
        cursor: pointer;
    }
`;

export const DarkCircle = css`
    position: absolute;
    position: absolute;
    left: 6px;
    right: 6px;
    top: 1px;
    bottom: 3px;
    background-color: rgba(0, 0, 0, 0.42);
    border-radius: 32px;
    color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left: 15px;
    padding-right: 15px;

    & > span {
        text-align: center;
    }
`;

export const AvaImg = css`
    width: 64px;
`;

export const DropButtonStyled = css`
    background-color: transparent;
    border: none;
`;

export const AvasGrid = css`
    display: grid;
    grid-template-columns: repeat(3, 80px);
    grid-template-rows: repeat(3, 80px);
    grid-template-areas:
        'ava0 ava0 ava0 ava0'
        'ava1 ava2 ava3 ava4'
        'ava5 ava6 ava7 ava8'
        'ava9 ava9 ava10 ava10';
`;

export const Button = css`
    background-color: #31af60;
    padding: 8px 12px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #fff;
    border: none;
    border-radius: 3px;
    margin-top: 24px;
    font-size: 14px;
`;

export const LottieWrapper = css`
    position: fixed;
    background: #fff;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    padding-top: 64px;
`;

export const WiteCirlce = css`
    background: #ffffff;
    top: 70px;
    left: calc(50% + 125px);
    position: absolute;
    width: 70px;
    height: 70px;
    z-index: 99;
`;
