import { getConfigValue } from '@ijl/cli';
import { useReducer } from 'react';

export enum SCENARIO_ELEMENTS {
    EMPTY = 'EMPTY',
    BIG_ANIM = 'BIG_ANIM',
    NOMINATION_ZANOVES = 'NOMINATION_ZANOVES',
    NOMINATION_AWARD = 'NOMINATION_AWARD',
}

import { lottieAnimationDatas } from '../../assets';

const presentsAnims = [
    lottieAnimationDatas.present1,
    lottieAnimationDatas.present2,
    lottieAnimationDatas.present3,
    lottieAnimationDatas.present4,
    lottieAnimationDatas.present5,
];
const goOverArray = array => {
    let i = -1;

    return () => {
        if (i < array.length - 1) {
            return array[++i];
        } else {
            i = 0;
            return array[i];
        }
    };
};
type Script = {
    stepsList: Array<string>;
    steps: Record<string, StepData>;
};

type StepData = {
    stage: SCENARIO_ELEMENTS;
    getAnimName?: () => any;
    username?: string;
    title?: string;
};
const END = 'end';
const openPrezentScenario: Script = {
    stepsList: ['open', END],
    steps: {
        open: {
            stage: SCENARIO_ELEMENTS.BIG_ANIM,
            getAnimName: goOverArray(presentsAnims),
        },
    },
};
const final: Script = {
    stepsList: ['open', END],
    steps: {
        open: {
            stage: SCENARIO_ELEMENTS.BIG_ANIM,
            getAnimName: () => lottieAnimationDatas.happyNewYear,
        },
    },
};
const getNominate = (title, useername, animName): Script => ({
    stepsList: ['open', 'award', END],
    steps: {
        open: {
            stage: SCENARIO_ELEMENTS.NOMINATION_ZANOVES,
            title,
            getAnimName: () => lottieAnimationDatas[animName],
        },
        award: {
            stage: SCENARIO_ELEMENTS.NOMINATION_AWARD,
            title,
            username: useername,
        },
    },
});

const scripts: Record<string, Script> = {
    presents: openPrezentScenario,
    final,
    nombeard: getNominate('Главная борода года', 'Bogdan', 'beard'),
    nomrisk: getNominate('Главный риск-менеджер года', 'Yulya', 'risk'),
    nomold: getNominate('Главный голос года', 'Rus', 'karaoke'),
    nompiper: getNominate('Главный трубопроводчик года', 'Vadim', 'pipe'),
    nombrain: getNominate('Главный мозг года', 'Andrey', 'brain'),
    nomdrank: getNominate('Главный пьяньчуга года', 'Vlad', 'drank'),
    nomgiant: getNominate('Главный великан года', 'Artem', 'giant'),
    nomsapper: getNominate('Главный сапёр года', 'Maks', 'sapper'),
    nomringleader: getNominate('Главный заводила года', 'Me', 'turnOn'),
    nomfichenator: getNominate('Главный фиченатор года', 'Igor', 'fichenator'),
    nomdeputat: getNominate('Главный депутат года', 'Petr', 'dep'),
};

type TState = {
    stage: SCENARIO_ELEMENTS;
    sc?: typeof openPrezentScenario;
    stepIndex?: number;
} & StepData;
const initialState = {
    stage: SCENARIO_ELEMENTS.EMPTY,
};
const postActionName = (isUserGolden, name) => {
    if (isUserGolden) {
        fetch(`${getConfigValue('zoom-bar.api')}/set-bar-event/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify({ type: name }),
        });
    }
};
export const useScenarios = isUserGolden => {
    const [state, dispatch] = useReducer((state: TState, action): TState => {
        switch (action.type) {
            case 'newScenario': {
                const nextScenario = scripts[action.value];
                if (!action.force) {
                    postActionName(
                        isUserGolden,
                        `start${action.value}Scenario`
                    );
                }
                if (action.force) {
                    const stepIndex = 0;
                    const nextStep =
                        nextScenario.steps[nextScenario.stepsList[stepIndex]];
                    return {
                        sc: nextScenario,
                        stepIndex,
                        ...nextStep,
                    };
                } else return state;
            }
            case 'reset': {
                if (!action.force) {
                    postActionName(isUserGolden, `reset`);
                }
                return initialState;
            }
            case 'next': {
                const { sc, stepIndex } = state;
                if (!action.force) {
                    postActionName(isUserGolden, 'next');
                }
                if (action.force) {
                    if (sc) {
                        const nextStepName = sc.stepsList[stepIndex + 1];
                        if (nextStepName === END) {
                            return initialState;
                        }

                        return {
                            sc: state.sc,
                            stepIndex: stepIndex + 1,
                            ...state.sc.steps[nextStepName],
                        };
                    } else return state;
                } else return state;
            }
        }
        return state;
    }, initialState);

    return {
        state,
        startpresentsScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'presents',
                force,
            }),
        startfinalScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'final',
                force,
            }),
        startnombeardScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nombeard',
                force,
            }),
        startnomriskScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomrisk',
                force,
            }),
        startnomoldScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomold',
                force,
            }),
        startnompiperScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nompiper',
                force,
            }),
        startnombrainScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nombrain',
                force,
            }),
        startnomdrankScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomdrank',
                force,
            }),
        startnomgiantScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomgiant',
                force,
            }),
        startnomsapperScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomsapper',
                force,
            }),
        startnomringleaderScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomringleader',
                force,
            }),
        startnomfichenatorScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomfichenator',
                force,
            }),
        startnomdeputatScenario: (force = false) =>
            dispatch({
                type: 'newScenario',
                value: 'nomdeputat',
                force,
            }),
        reset: (force = false) => dispatch({ type: 'reset', force }),
        next: (force = false) => dispatch({ type: 'next', force }),
    };
};
