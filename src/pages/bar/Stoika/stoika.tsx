import React from 'react';
import Lottie from 'react-lottie';

import { stoika } from '../../../assets';
import { Avatar } from '../Avatar';

import * as clss from './style';

export const Stoika = ({ online, hat }) => (
    <div className={clss.wrapper}>
        <Avatar
            hat={hat}
            onlineData={online}
            name="Yulya"
            isOnline={online['Yulya']?.online}
            index={3}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Bogdan"
            isOnline={online['Bogdan']?.online}
            index={3}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Igor"
            isOnline={online['Igor']?.online}
            index={3}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Vadim"
            isOnline={online['Vadim']?.online}
            index={3}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Rus"
            isOnline={online['Rus']?.online}
            index={3}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Artem"
            isOnline={online['Artem']?.online}
            index={3}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Vlad"
            isOnline={online['Vlad']?.online}
            index={3}
        />
        <img src={stoika} style={{ position: 'relative', zIndex: 2 }} />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Maks"
            isOnline={online['Maks']?.online}
            index={1}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Andrey"
            isOnline={online['Andrey']?.online}
            index={1}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Petr"
            isOnline={online['Petr']?.online}
            index={1}
        />
        <Avatar
            hat={hat}
            onlineData={online}
            name="Me"
            isOnline={online['Me']?.online}
            index={1}
        />
    </div>
);
