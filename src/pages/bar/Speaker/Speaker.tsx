import React from 'react';
import Lottie from 'react-lottie';

import { lottieAnimationDatas } from '../../../assets';

export const Speaker = ({ width = 150, height = 220, className = '' }) => (
    <div className={className}>
        <Lottie
            options={{
                loop: true,
                autoplay: true,
                animationData: lottieAnimationDatas.speaker,
                rendererSettings: {
                    preserveAspectRatio: 'xMidYMid slice',
                },
            }}
            eventListeners={[
                {
                    eventName: 'loopComplete',
                    callback: () => {},
                },
            ]}
            height={height}
            width={width}
        />
    </div>
);
