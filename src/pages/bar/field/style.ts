import { css } from '@emotion/css';

export const wrapper = css`
    padding: 8px 0 12px 10px;
    background-color: #fff;
    border: 2px solid #000000;
    border-radius: 7px;
`;

export const input = css`
    border: none;
    font-size: 24px;
    padding: 4px;

    &:focus,
    &:focus-visible {
        outline-width: 0;
    }
`;

export const submit = css`
    width: 64px;
    border: none;
    background-color: transparent;
    display: flex;
    align-items: center;
    justify-content: center;
`;
