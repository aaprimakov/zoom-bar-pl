import React, { useState } from 'react';
import clsx from 'clsx';

import * as clss from './style';

export const Field = ({ onSubmit, className }) => {
    const [value, setValue] = useState('');

    const handleChangevalue = event => {
        const v = event.target.value;

        setValue(v);
    };

    const handleSubmit = event => {
        event.preventDefault();

        onSubmit(value);
        setValue('');
    };

    return (
        <form className={clsx(clss.wrapper, className)} onSubmit={handleSubmit}>
            <input
                value={value}
                className={clss.input}
                onChange={handleChangevalue}
                placeholder="Type here"
            />
            <button type="submit" className={clss.submit}>
                <svg
                    stroke="#31AF60"
                    fill="#31AF60"
                    strokeWidth="0"
                    viewBox="0 0 16 16"
                    height="46px"
                    width="46px"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path d="m11.596 8.697-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z"></path>
                </svg>
            </button>
        </form>
    );
};
