import clsx from 'clsx';
import React from 'react';
import Lottie from 'react-lottie';

import {
    Igor,
    Petr,
    Vlad,
    Artem,
    Maks,
    Rus,
    Yulya,
    Bogdan,
    Me,
    Vadim,
    Andrey,
    lottieAnimationDatas,
} from '../../../assets';
import { useUser } from '../../../hooks/UseUser';

import * as clss from './style';

const avas = {
    Igor,
    Petr,
    Vlad,
    Artem,
    Maks,
    Rus,
    Yulya,
    Bogdan,
    Me,
    Vadim,
    Andrey,
};

type AvatarProps = {
    name: keyof typeof avas;
    index: number;
    isOnline?: boolean;
    className?: string;
    hat?: boolean;
    onlineData?: any;
};

export const Avatar: React.FC<AvatarProps> = ({
    name,
    index,
    isOnline,
    className,
    hat,
    onlineData,
}) => {
    const data = (onlineData || {})[name]?.data;
    const username = useUser();

    return (
        <div
            className={clsx(clss.wrapper, clss.places[name], className)}
            style={{ zIndex: index }}
        >
            {data?.chatMes && (
                <div className={clss.chatMes}>{data?.chatMes}</div>
            )}
            {data?.drink && (
                <div className={clss.drink}>
                    <Lottie
                        options={{
                            loop: false,
                            autoplay: true,
                            animationData: lottieAnimationDatas.drinkBeer,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid meet',
                            },
                        }}
                        height={75}
                        width={75}
                    />
                </div>
            )}
            {data?.confetti && (
                <div className={clss.confetti}>
                    <Lottie
                        options={{
                            loop: true,
                            autoplay: true,
                            animationData: lottieAnimationDatas.confetti3,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid meet',
                            },
                        }}
                        height={250}
                        width={250}
                    />
                </div>
            )}
            {data?.coffie && (
                <div className={clss.coffie}>
                    <Lottie
                        options={{
                            loop: true,
                            autoplay: true,
                            animationData: lottieAnimationDatas.coffie,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid meet',
                            },
                        }}
                        height={120}
                        width={120}
                    />
                </div>
            )}
            {((username === name && hat) || data?.hat) && (
                <div className={clss.hat}>
                    <Lottie
                        options={{
                            loop: true,
                            autoplay: true,
                            animationData: lottieAnimationDatas.christmasHat,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid meet',
                            },
                        }}
                        height={120}
                    />
                </div>
            )}
            {typeof isOnline !== 'undefined' && (
                <div className={clsx(clss.online, !isOnline && clss.offline)} />
            )}
            <img width={105} src={avas[name]} />
        </div>
    );
};
