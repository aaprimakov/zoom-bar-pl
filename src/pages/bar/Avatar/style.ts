import { css } from '@emotion/css';

export const wrapper = css`
    position: absolute;
    width: 105px;
    height: 105px;
    left: 50px;
    bottom: 50px;
`;

export const places = {
    Yulya: css`
        left: 15px;
        bottom: 139px;
    `,
    Bogdan: css`
        left: 151px;
        bottom: 139px;
    `,
    Igor: css`
        left: 279px;
        bottom: 139px;
    `,
    Vadim: css`
        left: 421px;
        bottom: 139px;
    `,
    Rus: css`
        left: 544px;
        bottom: 155px;
    `,
    Artem: css`
        left: 643px;
        bottom: 222px;
    `,
    Vlad: css`
        left: 699px;
        bottom: 337px;
    `,
    Maks: css`
        left: 96px;
        bottom: 295px;
    `,
    Andrey: css`
        left: 244px;
        bottom: 297px;
    `,
    Petr: css`
        left: 395px;
        bottom: 319px;
    `,
    Me: css`
        left: 575px;
        bottom: 394px;
    `,
};

export const online = css`
    position: absolute;
    right: 13px;
    top: 0px;
    width: 16px;
    height: 16px;
    background-color: #6eff62;
    border: 2px solid #c4c4c4;
    border-radius: 50%;
    box-sizing: border-box;
`;

export const offline = css`
    background-color: #ff6262;
`;

export const chatMes = css`
    color: #fff;
    position: absolute;
    left: 50%;
    transform: translate(-50%);
    width: 150px;
    text-align: center;
    top: -28px;
`;

export const hat = css`
    position: absolute;
    right: -7px;
    top: -19px;
    z-index: 2;
`;
export const coffie = css`
    position: absolute;
    right: -54px;
    top: -16px;
    z-index: 2;
`;
export const drink = css`
    position: absolute;
    right: 86px;
    top: -21px;
    z-index: 2;
`;
export const confetti = css`
    position: absolute;
    right: -75px;
    bottom: 100%;
    z-index: 2;
`;
