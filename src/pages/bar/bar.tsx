import React, { useMemo, useState, useRef, useEffect } from 'react';
import { getConfigValue } from '@ijl/cli';
import Lottie from 'react-lottie';
import clsx from 'clsx';

import { useBarEvents } from '../../hooks/events';
import { lottieAnimationDatas, scene, shtori, zanaves } from '../../assets';
import { useUser } from '../../hooks/UseUser';
import { GOLDEN_UERS } from '../../__data__/constants';

import { Speaker } from './Speaker';
import { Stoika } from './Stoika';
import * as clss from './style';
import { useScenarios, SCENARIO_ELEMENTS } from './hooks';
import { Avatar } from './Avatar';
import { Field } from './field';

const isZanavesOpened = (stage: SCENARIO_ELEMENTS) => {
    switch (stage) {
        case SCENARIO_ELEMENTS.NOMINATION_ZANOVES:
        case SCENARIO_ELEMENTS.NOMINATION_AWARD:
            return true;

        default:
            return false;
    }
};

const useUserControlls = () => {
    const timer = useRef(null);
    const timer2 = useRef(null);
    const timer3 = useRef(null);
    const [hat, setHat] = useState(false);
    const [coffie, setCoffie] = useState(false);
    const [drink, setDrinkBeer] = useState(false);
    const [chatMes, setChatMes] = useState();
    const [confetti, setConfetti] = useState(false);
    const username = useUser();
    const handleSetMessage = value => {
        clearTimeout(timer.current);
        timer.current = setTimeout(() => {
            setChatMes(null);
        }, 5000);
        setChatMes(value);
    };

    const handleSetConfetti = () => {
        clearTimeout(timer2.current);
        timer2.current = setTimeout(() => {
            setConfetti(null);
        }, 3000);
        setConfetti(true);
    };

    const handleDreenk = () => {
        clearTimeout(timer3.current);
        timer3.current = setTimeout(() => {
            setDrinkBeer(null);
        }, 8000);
        setDrinkBeer(true);
    };

    useEffect(() => {
        fetch(`${getConfigValue('zoom-bar.api')}/set-usr-event/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify({
                username,
                userData: { hat, chatMes, coffie, confetti, drink },
            }),
        });
    }, [hat, chatMes, coffie, confetti, drink]);

    return {
        hat,
        toggleHat: () => setHat(h => !h),
        setChatMes: handleSetMessage,
        toggleCoffie: () => setCoffie(c => !c),
        setConfettii: handleSetConfetti,
        handleDreenk,
    };
};

export const Bar = () => {
    const {
        hat,
        toggleHat,
        setChatMes,
        toggleCoffie,
        setConfettii,
        handleDreenk,
    } = useUserControlls();
    const user = useUser();
    const idRef = useRef<number>(0);
    const isUserGolden = GOLDEN_UERS.includes(user);

    const {
        state: { stage, getAnimName, title, username },
        ...actions
    } = useScenarios(isUserGolden);

    const { online, event, id } = useBarEvents();
    const animation = useMemo(() => {
        if (getAnimName) {
            return getAnimName();
        }
    }, [getAnimName]);

    useEffect(() => {
        if (event && id !== idRef.current) {
            idRef.current = id;
            actions[event](true);
        }
    }, [event, id]);

    return (
        <div className={clss.wrapper}>
            {stage === SCENARIO_ELEMENTS.BIG_ANIM && (
                <div className={clss.bigAnimWrapper}>
                    <Lottie
                        options={{
                            loop: true,
                            autoplay: true,
                            animationData: animation,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid meet',
                            },
                        }}
                        height={800}
                    />
                </div>
            )}
            {isUserGolden && (
                <div className={clss.controls}>
                    <button
                        className={clss.Button}
                        onClick={() => actions.startpresentsScenario()}
                    >
                        Подарок
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.reset()}
                    >
                        Reset
                    </button>

                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnombeardScenario()}
                    >
                        Борода года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomriskScenario()}
                    >
                        Риск менеджер года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomoldScenario()}
                    >
                        Голос года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnompiperScenario()}
                    >
                        Трубопроводчик года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnombrainScenario()}
                    >
                        Мозг года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomdrankScenario()}
                    >
                        Пьяньчуга года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomgiantScenario()}
                    >
                        Великан года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomsapperScenario()}
                    >
                        Сапёр года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomringleaderScenario()}
                    >
                        Главный заводила года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomfichenatorScenario()}
                    >
                        Фиченатор года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startnomdeputatScenario()}
                    >
                        Депутат года
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.next()}
                    >
                        Next
                    </button>
                    <button
                        className={clsx(clss.Button, clss.mt2)}
                        onClick={() => actions.startfinalScenario()}
                    >
                        Final
                    </button>
                </div>
            )}
            <img src={scene} className={clss.scene} />
            <div className={clss.xMassTree}>
                <Lottie
                    options={{
                        loop: true,
                        autoplay: true,
                        animationData: lottieAnimationDatas.xMassTree,
                        rendererSettings: {
                            preserveAspectRatio: 'xMidYMid meet',
                        },
                    }}
                    height={450}
                />
            </div>
            <img
                src={zanaves}
                className={clsx(
                    clss.zanaves1,
                    isZanavesOpened(stage) && clss.z1opened
                )}
            />
            <img
                src={zanaves}
                className={clsx(
                    clss.zanaves2,
                    isZanavesOpened(stage) && clss.z2opened
                )}
            />
            {title && <h2 className={clss.nominationTitle}>{title}</h2>}
            {isZanavesOpened && (
                <div className={clss.nomAnim}>
                    <Lottie
                        options={{
                            loop: true,
                            autoplay: true,
                            animationData: animation,
                            rendererSettings: {
                                preserveAspectRatio: 'xMidYMid meet',
                            },
                        }}
                        height={250}
                    />
                </div>
            )}
            {isZanavesOpened && username && (
                <>
                    <div className={clss.nomAnim} style={{ zIndex: 10 }}>
                        <Lottie
                            options={{
                                loop: true,
                                autoplay: true,
                                animationData: lottieAnimationDatas.confetti,
                                rendererSettings: {
                                    preserveAspectRatio: 'xMidYMid meet',
                                },
                            }}
                            height={450}
                        />
                        <Lottie
                            options={{
                                loop: true,
                                autoplay: true,
                                animationData: lottieAnimationDatas.confetti2,
                                rendererSettings: {
                                    preserveAspectRatio: 'xMidYMid meet',
                                },
                            }}
                            height={600}
                        />
                    </div>
                    <Avatar
                        name={username as 'Igor'}
                        className={clss.sceneAvatar}
                        index={2}
                    />
                </>
            )}
            <img src={shtori} className={clss.kulisi} />

            <Speaker className={clss.speaker1} />
            <Speaker className={clss.speaker2} height={500} width={300} />
            <Speaker className={clss.speaker3} />

            <Stoika hat={hat} online={online} />

            <div className={clss.userControls}>
                <button
                    className={clsx(clss.Button, clss.mr2)}
                    onClick={handleDreenk}
                >
                    Выпить
                </button>
                <button
                    className={clsx(clss.Button, clss.mr2)}
                    onClick={setConfettii}
                >
                    Хлопушка
                </button>
                <button
                    className={clsx(clss.Button, clss.mr2)}
                    onClick={toggleCoffie}
                >
                    Кофе
                </button>
                <button className={clss.Button} onClick={toggleHat}>
                    Шапка
                </button>
            </div>

            <Field className={clss.field} onSubmit={setChatMes} />
        </div>
    );
};
