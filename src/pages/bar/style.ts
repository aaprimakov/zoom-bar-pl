import { css } from '@emotion/css';

const ANIMATION_DURATION = '1.2s';

export const wrapper = css`
    background-color: #313131;
    height: 100vh;
    width: 100vw;
    position: relative;
`;

export const speaker1 = css`
    position: absolute;
    left: 64px;
    top: 64px;

    @media only screen and (max-width: 1150px) {
        display: none;
    }
`;
export const speaker2 = css`
    position: absolute;
    left: 24px;
    top: 80px;
    @media only screen and (max-width: 1200px) {
        display: none;
    }
`;
export const speaker3 = css`
    position: absolute;
    right: 64px;
    top: 64px;

    @media only screen and (max-width: 1050px) {
        display: none;
    }
`;

export const scene = css`
    position: absolute;
    left: 50%;
    transform: translate(-50%);
    top: 92px;
    z-index: 2;
`;
export const kulisi = css`
    position: absolute;
    left: 50%;
    transform: translate(-50%);
    top: 12px;
    z-index: 4;
`;

export const zanaves1 = css`
    position: absolute;
    left: 50%;
    transform: translate(calc(-100% + 30px));
    top: 12px;
    transition: transform ${ANIMATION_DURATION};
    transform-origin: center left;
    z-index: 3;
`;

export const zanaves2 = css`
    transform-origin: center left;
    position: absolute;
    left: 50%;
    transform: translate(-30px);
    top: 12px;
    transition: transform ${ANIMATION_DURATION};
    z-index: 3;
`;

export const z1opened = css`
    transform: translate(calc(-100% + 30px)) scaleX(0.2);
`;

export const z2opened = css`
    transform: translate(270px) scaleX(0.2);
`;

export const Button = css`
    background-color: #31af60;
    padding: 8px 12px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #fff;
    border: none;
    border-radius: 3px;
    font-size: 14px;
`;

export const mt2 = css`
    margin-top: 12px;
`;
export const mr2 = css`
    margin-right: 12px;
`;

export const controls = css`
    position: absolute;
    left: 50px;
    top: 12px;
    background-color: #00000026;
    padding: 24px;
    z-index: 99999;
`;

export const bigAnimWrapper = css`
    position: fixed;
    z-index: 100;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #000000bf;
`;

export const nominationTitle = css`
    position: absolute;
    left: 50%;
    transform: translate(-50%);
    color: #ffffff;
    z-index: 2;
`;

export const nomAnim = css`
    position: absolute;
    left: 50%;
    top: 64px;
    transform: translate(-50%);
    z-index: 2;
`;

export const sceneAvatar = css`
    top: 85px;
    left: 50%;
    transform: translate(-50%);
    width: auto;

    & img {
        width: 193px;
    }
`;

export const userControls = css`
    position: absolute;
    display: flex;
    right: 50px;
    bottom: 12px;
    background-color: #00000026;
    padding: 24px;
    z-index: 99999;
`;

export const field = css`
    position: absolute;
    display: flex;
    left: 50px;
    bottom: 12px;
    z-index: 99999;
`;

export const xMassTree = css`
    position: absolute;
    left: 0;
    top: 475px;
    z-index: 2;

    @media only screen and (max-width: 1050px) {
        display: none;
    }
`;
