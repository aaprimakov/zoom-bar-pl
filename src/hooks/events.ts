import { useState, useEffect } from 'react';
import { getConfigValue } from '@ijl/cli';
import { LOCALSTORAGE_NICK_NAME } from '../__data__/constants';

type Name = string;
type UsersInfo = { online: Record<Name, { online: boolean; dataa: any }> };
const INTERVAL = 2200;

export const useEvents = (): UsersInfo => {
    const [data, setData] = useState({ online: {} });
    useEffect(() => {
        const timer = setInterval(async () => {
            const answer = await fetch(
                `${getConfigValue('zoom-bar.api')}/events`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8',
                    },
                }
            );
            const data = await answer.json();
            setData(data);
        }, INTERVAL);

        return () => {
            clearInterval(timer);
        };
    }, []);

    return data;
};

export const useBarEvents = () => {
    const [data, setData] = useState<{
        online: Record<string, boolean>;
        event?: string;
        id?: number;
    }>({
        online: {},
    });
    useEffect(() => {
        const timer = setInterval(async () => {
            const answer = await fetch(
                `${getConfigValue('zoom-bar.api')}/bar/${localStorage.getItem(
                    LOCALSTORAGE_NICK_NAME
                )}`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8',
                    },
                }
            );
            const data = await answer.json();
            setData(data);
        }, INTERVAL);

        return () => {
            clearInterval(timer);
            fetch(`${getConfigValue('zoom-bar.api')}/quit-bar`);
        };
    }, []);
    return data;
};
