import { useState } from 'react';

import { LOCALSTORAGE_NICK_NAME } from '../__data__/constants';

export const useUser = () => {
    const [user] = useState<any>(localStorage.getItem(LOCALSTORAGE_NICK_NAME));

    return user;
};
