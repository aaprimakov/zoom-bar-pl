import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import { URLs } from './__data__/urls';
import { Auth, Bar } from './pages';

export const Dashboard = () => {
    return (
        <Routes>
            <Route
                path={URLs.baseUrl}
                element={<Navigate replace to={URLs.auth.url} />}
            />
            <Route path={URLs.auth.url} element={<Auth />} />
            <Route path={URLs.bar.url} element={<Bar />} />
        </Routes>
    );
};
