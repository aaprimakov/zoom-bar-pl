export { default as Andrey } from './avas/Andrey.png';
export { default as Igor } from './avas/Igor.png';
export { default as Petr } from './avas/Petr.png';
export { default as Vlad } from './avas/Vlad.png';
export { default as Artem } from './avas/Artem.png';
export { default as Maks } from './avas/Maks.png';
export { default as Rus } from './avas/Rus.png';
export { default as Yulya } from './avas/Yulya.png';
export { default as Bogdan } from './avas/Bogdan.png';
export { default as Me } from './avas/Me.png';
export { default as Vadim } from './avas/Vadim.png';

export { default as scene } from './pics/scene.png';
export { default as shtori } from './pics/shtori.png';
export { default as zanaves } from './pics/zanaves.png';
export { default as stoika } from './pics/stoika.png';

import { default as friday } from './lottie/Happy Friday.json';
import { default as speaker } from './lottie/Speaker Animation Reveal.json';
import { default as present1 } from './lottie/present-birthday.json';
import { default as present2 } from './lottie/present-2.json';
import { default as present3 } from './lottie/present-3.json';
import { default as present4 } from './lottie/present-4.json';
import { default as present5 } from './lottie/present-5.json';

import { default as confetti } from './lottie/CONFETTI.json';
import { default as confetti2 } from './lottie/confetti2.json';
import { default as beard } from './lottie/beard.json';
import { default as light } from './lottie/light.json';
import { default as risk } from './lottie/risk.json';
import { default as karaoke } from './lottie/karaoke.json';
import { default as pipe } from './lottie/pipe.json';
import { default as brain } from './lottie/brain.json';
import { default as drank } from './lottie/drank.json';
import { default as giant } from './lottie/giant.json';
import { default as multitask } from './lottie/multitask.json';
import { default as sapper } from './lottie/sapper.json';
import { default as turnOn } from './lottie/turn-on.json';
import { default as fichenator } from './lottie/fichenator.json';
import { default as dep } from './lottie/dep.json';
import { default as christmasHat } from './lottie/christmas-hat.json';
import { default as xMassTree } from './lottie/x-mass-tree.json';
import { default as coffie } from './lottie/coffie.json';
import { default as happyNewYear } from './lottie/happy-new-year.json';
import { default as confetti3 } from './lottie/confetti-3.json';
import { default as drinkBeer } from './lottie/drink-beer.json';

export const lottieAnimationDatas = {
    friday,
    speaker,
    present1,
    present2,
    present3,
    present4,
    present5,
    confetti,
    confetti2,
    beard,
    light,
    risk,
    karaoke,
    pipe,
    brain,
    drank,
    giant,
    sapper,
    turnOn,
    fichenator,
    dep,
    christmasHat,
    xMassTree,
    coffie,
    happyNewYear,
    confetti3,
    drinkBeer,
};
