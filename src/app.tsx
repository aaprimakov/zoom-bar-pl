import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { Dashboard } from './dashboard';

const App = () => {
    return (
        <React.StrictMode>
            <BrowserRouter>
                <Dashboard />
            </BrowserRouter>
        </React.StrictMode>
    );
};

export default App;
