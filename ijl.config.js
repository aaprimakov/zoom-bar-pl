const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        'zoom-bar.main': '/zoom-bar',
        'link.zoom-bar.auth': '/auth',
        'link.zoom-bar.bar': '/launge',
    },
    features: {
        'zoom-bar': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        'zoom-bar.api': '/api',
        'zoom-bar.zoom.link': 'https://zoom.com',
        'zoom-bar.croco.link': 'https://croco.com'
    }
}