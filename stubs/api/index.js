const router = require('express').Router();

const onlines = new Map([
    ['Artem', {
        online: true
    }],
    ['Bogdan', {
        online: false
    }],
    ['Vadim', {
        online: true
    }],
    ['Andrey', {
        online: true
    }],
]);

const otmetki = new Map();

setInterval(() => {
    // console.log('interval', otmetki, onlines);
    [...otmetki.entries()].forEach(([user, otm]) => {
        if (Date.now() - otm > 3500) {
            onlines.set(user, {
                ...(onlines.get(user) || {}),
                online: false
            })
        }
    })
}, 3000);

const mapToObj = (map) => [...map.entries()].reduce((acc, [key, value]) => ({
    ...acc,
    [key]: {
        name: key,
        ...value
    }
}), {})

router.post('/enter', (req, res) => {
    const username = req.body.username;
    onlines.set(username, {
        ...(onlines.get(username) || {}),
        online: true
    });
    req.session.username = username;
    res.send({});
});

let syncData = {
    online: mapToObj(onlines)
}

router.get('/events', (req, res) => {
    syncData.online = mapToObj(onlines);
    res.send(syncData);
});

router.post('/set-usr-event', (req, res) => {
    const {
        username,
        userData
    } = req.body;
    onlines.set(username, {
        ...(onlines.get(username) || {}),
        data: userData
    })
    res.send({});
})

router.post('/set-bar-event', (req, res) => {
    const {
        type
    } = req.body;
    syncData.event = type;
    syncData.id = Math.random();

    res.send({});
});

router.get('/bar/:u', (req, res) => {
    onlines.set(req.params.u, {
        ...(onlines.get(req.params.u) || {}),
        online: true
    });
    syncData.online = mapToObj(onlines);
    // syncData.id = Math.random();
    otmetki.set(req.params.u, Date.now())

    res.send(syncData);
});

router.get('/quit-bar', (req, res) => {
    onlines.set(req.session.username || req.params.u, {
        ...(onlines.get(req.session.username || req.params.u) || {}),
        online: false
    });
    syncData.online = mapToObj(onlines);
    res.send({});
})

module.exports = router;